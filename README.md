Initial setup:

1. `pip install virtualenv`
2. `virtualenv -p python3 env`
3. `source env/bin/activate`
4. `pip install -r requirements.txt`

Running:

1. `python app/app.py`

Every pdf file over 50MB inside folder will be splitted in smaller pieces. 