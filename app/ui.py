import tkinter as tk
from tkinter import filedialog

import pygubu
from app import pdf_splitter


class Application:
    def __init__(self, master):
        self.master = master
        self.builder = builder = pygubu.Builder()
        builder.add_from_file('app/ui.ui')
        self.splitter = builder.get_object('splitter', master)
        builder.connect_callbacks(self)
        self.find_button = builder.get_object('find_button', master)
        self.split_button = builder.get_object('split_button', master)
        self.directory_path = builder.get_object('directory_path')
        self.message_field = builder.get_object('message_field')

    def find_callback(self):
        self.builder.tkvariables['message_var'].set('')
        self.folder_selected = filedialog.askdirectory()
        self.directory_path.insert(0, self.folder_selected)

    def split_callback(self):
        print('Splitting all PDF files in this directory...')
        response = pdf_splitter(self.folder_selected)
        print(response)
        self.builder.tkvariables['message_var'].set(response)


if __name__ == '__main__':
    root = tk.Tk()
    app = Application(root)

    root.mainloop()
