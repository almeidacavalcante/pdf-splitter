import os
import glob
from PyPDF2 import PdfFileReader, PdfFileWriter
from page_iterator import page_iterator
from pages_per_file import pages_per_file


def pdf_splitter(path, MAX_SIZE=50000000, RATE=0.97):  # under 50 MB
    ''' 
        path: file name path
        MAX_SIZE: file pdf maximum size (default value is under 50MB)
        RATE: when a part of a pdf is over 50MB, there's a reduction in
        number of pages to acomodate the part inside this 50MB size. If
        100 pages gives you 51MB, the app will try to create anothe
        PDF with 98 pages (0.98 * 100 pages) and evaluate again...
    '''

    for file in os.listdir(path):
        file_path = path + '/' + file
        if file.endswith('.pdf') and os.stat(file_path).st_size > MAX_SIZE:
            fname = os.path.splitext(os.path.basename(file_path))[0]
            pdf = PdfFileReader(file_path)  # complete file
            org_file_size = os.stat(file_path).st_size

            remaining_pages = pdf.getNumPages()  # initally iquals to the total
            iterator = page_iterator(remaining_pages)
            reduction_rate = 1  # 1 as rate causes no reduction
            last_page = 0
            ppf = pages_per_file(remaining_pages, org_file_size,
                                 MAX_SIZE, reduction_rate)
            pages_per_part = ppf.get_estimated_pages_per_part()
            print('PPP: {}'.format(pages_per_part))
            # Setting the OUTPUT FOLDER relative to the project root folder
            os.chdir(os.getcwd() + '/folder/output/')

            try:
                while remaining_pages > 0:
                    if remaining_pages >= pages_per_part:
                        pages_per_part = ppf.get_estimated_pages_per_part(
                            reduction_rate)
                    else:
                        pages_per_part = remaining_pages

                    pdf_writer = PdfFileWriter()
                    for page in range(pages_per_part):
                        pdf_writer.addPage(pdf.getPage(iterator.next()))

                    # generate the output file giving the last page as a sufix
                    out_filename = '{}_to_page_{}.pdf'.format(
                        fname, iterator.last_number())

                    with open(out_filename, 'wb') as out:
                        pdf_writer.write(out)
                        if os.stat(out_filename).st_size > MAX_SIZE:
                            print('Testing part size: {}'.format(
                                os.stat(out_filename).st_size))
                            print('Pages of each part: {}'.format(pages_per_part))

                            os.remove(out_filename)  # Removing the file
                            reduction_rate = reduction_rate * RATE
                            iterator.reset_to(last_page)

                        else:
                            print('Created: {}'.format(out_filename))
                            remaining_pages = remaining_pages - pages_per_part
                            last_page = iterator.last_number()

                os.chdir('../../')

            except expression as identifier:
                print('ERROR!')
                import pdb
                pdb.set_trace()

    return 'Finished Successfully! All PDF files have been splitted!'


if __name__ == '__main__':
    path = 'folder'
    pdf_splitter(path)
