class pages_per_file:
    def __init__(self, remaining_pages, org_file_size, max_size, reduction_rate):
        self.reduction_rate = reduction_rate
        self.size_by_page_ratio = org_file_size / remaining_pages
        self.estimate_pages_per_part = max_size / self.size_by_page_ratio

    def get_estimated_pages_per_part(self, new_rate=None):
        if self.size_by_page_ratio and new_rate == None:
            pages = int(self.estimate_pages_per_part * self.reduction_rate)
            return pages
        elif new_rate:
            self.reduction_rate = new_rate
            pages = int(self.estimate_pages_per_part * self.reduction_rate)
            return pages

        return self.estimate_pages_per_part
